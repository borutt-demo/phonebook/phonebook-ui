# Phone book UI project

This project is a front-end part of a phone book client-service projects pair. The project consists of Java EE code. The user interface is web interface based on PrimeFaces framework. Front end depends on the phone book back-end service via RESTful communication.

> This project is intended solely for testing and demonstration purposes.

> [Related Phone book back-end project](https://gitlab.com/borutt-demo/phonebook/phonebook-backend)

## Getting Started
The following instructions let you build the project's Java EE war artifact. Subsequent instructions regarding the deployment environment configuration need to be followed in the related back-end project.

This project has compile-time dependency on the API part of the back-end project. It has run-time dependency on the implementation part of the back-end project.

### Requirements
* [Maven](https://maven.apache.org/) 3.6.0 or newer
* JDK 1.8.x
* [Wildfly](http://wildfly.org/downloads/) 8 or newer or other fully Java EE 7 compatible application server (tested with Wildfly 13.0.0)
* Phone book back-end artifact of specific version installed in a local Maven repository

The Maven repository phone book back-end artifact version must correspond to the version of the phone back-end artifact dependency in this project's `pom.xml` file. The phone book back-end artifact with version 1.0.0 is defined as:

```
<groupId>org.borutt</groupId>
<artifactId>phonebook-backend</artifactId>
<version>1.0.0</version>
```

## Building and Installing

### Maven

Build WAR artifact from source code and install it into a local Maven repository:

    $ ./mvn clean install

> The above command also creates a Javadoc JAR artifact and a source code JAR artifact. All created artifacts become available in a local Maven repository and in the project's subdirectory `./target`.


## Deploying to application server

### Back-end service endpoint configuration

The built phone book UI war artifact contains a preconfigured RESTful service back-end endpoint, located in `WEB-INF/web.xml`. 

```
...
    <env-entry>
        <env-entry-name>phonebookApiEndpoint</env-entry-name>
        <env-entry-type>java.lang.String</env-entry-type>
        <env-entry-value>http://localhost:8080/ImenikApi/phonebook</env-entry-value>
    </env-entry>
...
```
The above excerpt shows the preconfigured enpoint URL entry `http://localhost:8080/ImenikApi/phonebook` to address the RESTful back-end service, where the scheme `http`, host `localhost` and port `8080` may be freely changed according to the custom deployment environment configuration.

Currently no other means of configuring the back-end service endpoint are supported.

### Application server configuration

The built war artifact may be deployed to any application server that fully supports Java EE 7 specification. Please refer to the [Phone book back-end artifact deployment instructions](https://gitlab.com/borutt-demo/phonebook/phonebook-backend/blob/master/README.md#deploying-to-application-server) to deploy this artifact. Continue with the application usage instruction after the war artifacts have been successfully deployed. 

> This guide assumes that both artifacts (UI and backend) share the same application server instance.

### Application usage

After both, phone book UI and back-end war artifacts, have been successfully deployed, test the application by entering the address `http://localhost:8080/TelefonskiImenik/` into a browser.

> Optionally change the server host and port according to your system configuration. 

## Authors
* Borut Turšič
  