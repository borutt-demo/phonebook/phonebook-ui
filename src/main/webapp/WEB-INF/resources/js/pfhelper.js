/**
 * Prevents PrimeFaces dialog from hiding in case of validation error. 
 * See https://stackoverflow.com/questions/9195756/keep-pdialog-open-when-a-validation-error-occurs-after-submit
 */
$(document).ready(function() {
	// Keeps a reference to the original hide().
    PrimeFaces.widget.Dialog.prototype.originalHide = PrimeFaces.widget.Dialog.prototype.hide; 
    PrimeFaces.widget.Dialog.prototype.hide = function() {
    	// Accesses oncomplete arguments.
        var ajaxResponseArgs = arguments.callee.caller.arguments[2];
        if (ajaxResponseArgs && ajaxResponseArgs.validationFailed) {
        	// Prevents closing on validation error. 
            return;  
        }
        this.originalHide();
    };
});