package org.borutt.phonebook.client;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.ws.rs.core.UriBuilder;

import org.borutt.api.contact.ContactResource;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

@ApplicationScoped
public class RestClientFactory {

    private ResteasyWebTarget phonebookTarget;

    // Resource is configured in web.xml
    @Resource(name = "phonebookApiEndpoint")
    private String phonebookApiEndpoint;

    @PostConstruct
    public void init() {
        ResteasyClient client = new ResteasyClientBuilder().build();
        phonebookTarget = client.target(UriBuilder.fromPath(phonebookApiEndpoint));
    }

    @Produces
    public ContactResource produceContactResourceClient() {
        return phonebookTarget.proxy(ContactResource.class);
    }

}
