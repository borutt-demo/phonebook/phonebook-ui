package org.borutt.phonebook.ui;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

@Named("localization")
@SessionScoped
public class LocalizationBean implements Serializable {

    private static final long serialVersionUID = 1L;

    // language, locale
    private static final Map<String, Locale> languagesToLocales;

    // Locale language tag
    private String selectedLocaleId;

    static {
        Map<String, Locale> initLocales = new LinkedHashMap<>();
        initLocales.put("English", Locale.ENGLISH);
        initLocales.put("Slovenščina", Locale.forLanguageTag("sl-SI"));
        languagesToLocales = Collections.unmodifiableMap(initLocales);
    }

    public Map<String, Locale> getLanguagesToLocales() {
        return languagesToLocales;
    }

    public String getSelectedLocaleId() {
        return selectedLocaleId;
    }

    public void setSelectedLocaleId(String localeId) {
        this.selectedLocaleId = localeId;
    }

    // Value change event listener
    public void notifyLocaleCodeChanged(ValueChangeEvent e) {

        String newLocaleValue = e.getNewValue().toString();

        // Loop country map to compare the locale code
        for (Map.Entry<String, Locale> entry : languagesToLocales.entrySet()) {
            if (entry.getValue().toString().equals(newLocaleValue)) {
                FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale) entry.getValue());
            }
        }
    }

}
