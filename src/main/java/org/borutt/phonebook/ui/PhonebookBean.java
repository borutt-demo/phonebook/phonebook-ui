package org.borutt.phonebook.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.borutt.api.contact.Contact;
import org.borutt.api.contact.ContactResource;

@Named(value = "phonebook")
@ViewScoped
public class PhonebookBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private ContactResource contactResource;

    private Contact contact;
    private List<Contact> contacts = new ArrayList<>();

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    @PostConstruct
    public void init() {
        populateContacts();
    }

    public void populateContacts() {
        contacts = contactResource.findAll();
    }

    public void prepareEmptyContact() {
        contact = new Contact();
    }

    /**
     * Modifications made to the prepared contact instance do not reflect
     * modifications to the original instance in case the editing process is
     * canceled.
     * 
     * @param original Original contact instance to be copied for reversible
     *                 editing.
     */
    public void prepareContactForEditing(Contact original) {
        this.contact = new Contact(original);
    }

    public void saveContact() {
        if (contact.getId() != null && contact.getId() >= 0) {
            contactResource.update(contact);
        } else {
            contactResource.create(contact);
        }
    }

    public void deleteContact(Long id) {
        if (id == null || id < 0) {
            throw new IllegalStateException("Contact to be deleted DB does not exist.");
        }
        contactResource.delete(id);
    }

}