package org.borutt.phonebook.ui;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

@ApplicationScoped
public class WebMessaging {

    private static final String PHONEBOOK_MSG_PROPERTIES_PATH = "org/borutt/lang/phonebook.properties";

    private Properties properties;

    @PostConstruct
    public void loadMessageProperties() {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            properties = new Properties();
            properties.load(classLoader.getResourceAsStream(PHONEBOOK_MSG_PROPERTIES_PATH));
        } catch (IOException e) {
            throw new RuntimeException("Error loading language properties file.", e);
        }

    }

    public void addWarnMsgProperty(String msgPropertyName, String componentId) {
        addWarnMessage(properties.getProperty(msgPropertyName), componentId);
    }

    public void addWarnMessage(String detail, String componentId) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN, detail, null);
        FacesContext.getCurrentInstance().addMessage(componentId, message);
    }

}
